//
//  ApiService.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation
import Alamofire

struct ApiService {
    
    static let shared = ApiService()
    let apiConstant = ApiConstant.self
    
    func requestLogin(params: Parameters, completionHandler completion:((_ status: Result<Any>, _ errorMessage: String, _ results:NSDictionary?) -> Void)? = nil) {
        let apiRequest = apiConstant.login(params: params)
        ApiManager.create(withURL: apiRequest.fullUrl, byMethod: apiRequest.apiMethod, parameters: apiRequest.parameters, encoding: apiRequest.apiEncoding, headers: apiRequest.headers, withAction: { }) { (status, errorMsg, results) in
            switch status {
            case .success:
                guard let result = results as? NSDictionary else {
                    completion?(status,errorMsg,nil)
                    return
                }
                
                completion?(status,errorMsg,result)
                
            case .failure:
                completion?(status,errorMsg,nil)
            }
        }
    }
    
    func requestProfile(idUser: String, completionHandler completion:((_ status: Result<Any>, _ errorMessage: String, _ results:NSDictionary?) -> Void)? = nil) {
        let apiRequest = apiConstant.profile(idUser: idUser)
        ApiManager.create(withURL: apiRequest.fullUrl, byMethod: apiRequest.apiMethod, parameters: apiRequest.parameters, encoding: apiRequest.apiEncoding, headers: apiRequest.headers, withAction: { }) { (status, errorMsg, results) in
            switch status {
            case .success:
                guard let result = results as? NSDictionary else {
                    completion?(status,errorMsg,nil)
                    return
                }
                
                completion?(status,errorMsg,result)
                
            case .failure:
                completion?(status,errorMsg,nil)
            }
        }
    }
    
}
