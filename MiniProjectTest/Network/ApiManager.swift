//
//  ApiManager.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation
import Alamofire

class ApiManager {
    
    class func create(
        withURL url:String, byMethod methods: HTTPMethod,
        parameters:[String:Any]?=nil, encoding:ParameterEncoding,
        headers: [String:String]?,
        withAction action:(() -> Void)? = nil,
        completionHandler completion:((_ status: Result<Any>, _ errorMessage: String, _ results:[String:Any]?) -> Void)? = nil
    ){
      
        var head = HTTPHeaders()
        if let header = headers {
            head = header
        } else {
            head = [:]
        }
        let param = parameters
        
        print("URL ",url)
        print("PARAM ",parameters)
        print("HEADER ",head)
        
        DispatchQueue.main.async {
            action?()
        }
        
        
        Alamofire.request(
            url, method: methods, parameters: param,
            encoding: encoding, headers: head).validate(statusCode: 200..<500).responseJSON { response in
            print("RESPONSE ",response)
            switch response.result{
            case .success(let jsonData):
                guard let json = jsonData as? [String:Any] else{
                    DispatchQueue.main.async {
                        completion?(response.result,"",nil)
                    }
                    return
                }
                DispatchQueue.main.async {
                    completion?(response.result,"",json)
                }
                
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    print("THIS THE ERRORRRR FAILURE ",error)
                    completion?(response.result,error.localizedDescription,nil)
                }
                break
            }
        }
        
    }
    
}
