//
//  ApiConstant.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation
import Alamofire

enum ApiConstant {
    case login(params: [String:Any])
    case profile(idUser: String)
}

extension ApiConstant {
    
    var fullUrl: String {
        return ApiConfig.baseUrl + pathEndpoint
    }
    
    var pathEndpoint: String {
        switch self {
        case .login:
            return ApiConfig.login
        case .profile(let idUser):
            return ApiConfig.profile + idUser
        }
    }
    
    var apiMethod: HTTPMethod {
        switch self {
        case .login:
            return .post
        case .profile:
            return .get
        }
    }
    
    var parameters: [String:Any] {
        switch self {
        case .login(let params):
            var parameters = [String: Any]()
            parameters = params
            return parameters
        case .profile:
            return [:]
        }
    }
    
    var apiEncoding: ParameterEncoding {
        switch self {
        case .login:
            return JSONEncoding.default
        case .profile:
            return URLEncoding.default
        }
    }
    
    var headers: [String: String]? {
        switch self {
        case .login,
                .profile:
            return [:]
        }
    }
    
}
