//
//  ApiConfig.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation

struct ApiConfig {
    
    static let baseUrl = "https://reqres.in/api"
    
    // MARK: - AUTH
    static let login = "/register"
    
    // MARK: - ACCOUNT
    static let profile = "/users/"
    
}
