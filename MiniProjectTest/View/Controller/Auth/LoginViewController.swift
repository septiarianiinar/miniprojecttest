//
//  LoginViewController.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import UIKit
import PKHUD
import Toast_Swift
import TextFieldEffects

class LoginViewController: UIViewController {

    @IBOutlet var emailField: HoshiTextField!
    @IBOutlet var passwordField: HoshiTextField!
    @IBOutlet var loginBtn: UIButton!
    
    let viewModel = LoginViewModel(apiService: ApiService())
    var isLoad:Bool = false
    
    // MARK: - Overrides
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loginBtn.layer.cornerRadius = 8.0
        loginBtn.addTarget(self, action: #selector(loginBtnTapped), for: .touchDown)
    }
    
    // MARK: - Privates
    private func loadingStart() {
        HUD.show(.progress)
        isLoad = true
        self.setupLoadingView()
    }
    
    private func loadingStop() {
        HUD.hide()
        isLoad = false
        self.setupLoadingView()
    }
    
    private func setupLoadingView() {
        emailField.isEnabled = !isLoad
        passwordField.isEnabled = !isLoad
        loginBtn.isEnabled = !isLoad
    }

    // MARK: - Actions
    @objc private func loginBtnTapped() {
        guard let email = emailField.text, email != "" else {
            self.view.makeToast("Email is Required", duration: 1.8, position: .bottom)
            return
        }
        
        guard let password = passwordField.text, email != "" else {
            self.view.makeToast("Password is Required", duration: 1.8, position: .bottom)
            return
        }
        
        let params: [String:Any] = ["email":email,
                                    "password":password]
        self.fetchLogin(params: params)
    }
    
    // MARK: - Networking
    private func fetchLogin(params: [String:Any]) {
        viewModel.fetchLogin(params: params)
        
        viewModel.updateLoading = {
            let _ = self.viewModel.isLoading ? self.loadingStart() : self.loadingStop()
        }
        
        viewModel.showAlert = {
            if let errorMsg = self.viewModel.errorMessage {
                self.view.makeToast(errorMsg, duration: 1.5, position: .bottom)
            }
        }
        
        viewModel.didFinishFetchLogin = {
            self.emailField.text = ""
            self.passwordField.text = ""
            self.view.makeToast("Login success", duration: 2.0, position: .bottom)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                let navVC = UINavigationController(rootViewController: vc)
                vc.modalPresentationStyle = .fullScreen
                navVC.modalPresentationStyle = .fullScreen
                self.present(navVC, animated: true, completion: nil)
            }
        }
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
}
