//
//  HomeViewController.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import UIKit
import PKHUD
import Toast_Swift
import AlamofireImage

class HomeViewController: UIViewController {

    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var userFullnameLabel: UILabel!
    @IBOutlet var userEmailLabel: UILabel!
    
    let viewModel = HomeViewModel(apiService: ApiService())
    var isLoad:Bool = false
    
    // MARK: - Overrides
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Home"
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationItem.title = ""
        
        let itemButtonSize:CGFloat = (self.navigationController?.navigationBar.frame.height ?? 0.0)
        let logoutBtn = UIButton()
        logoutBtn.setTitle("Logout", for: .normal)
        logoutBtn.setTitleColor(UIColor.red, for: .normal)
        logoutBtn.addTarget(self, action: #selector(self.logoutBtnTapped), for: .touchUpInside)
        logoutBtn.contentMode = .scaleAspectFit
        logoutBtn.frame = CGRect(x: 0, y: 0, width: 88.0, height: itemButtonSize)
        let logoutButton = UIBarButtonItem.init()
        logoutButton.customView = logoutBtn
        
        self.navigationItem.rightBarButtonItems = [logoutButton]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.userImageView.layer.cornerRadius = self.userImageView.frame.height / 2
        
        guard let loginData = AppData.loginData, let idUser = loginData.id else { return }
        self.fetchProfileData(idUser: String(idUser))
    }
    
    // MARK: - Privates
    private func loadingStart() {
        HUD.show(.progress)
        isLoad = true
        self.setupLoadingView()
    }
    
    private func loadingStop() {
        HUD.hide()
        isLoad = false
        self.setupLoadingView()
    }
    
    private func setupLoadingView() {
        self.navigationController?.navigationBar.isUserInteractionEnabled = !isLoad
    }
    
    private func updateDataView() {
        guard let data = viewModel.profileData else {
            self.view.makeToast("Profile Data not Found", duration: 2.0, position: .bottom)
            return
        }
        
        self.userFullnameLabel.text = (data.data?.first_name ?? "") + " " + (data.data?.last_name ?? "")
        self.userEmailLabel.text = data.data?.email ?? "-"
        
        if let avatar = data.data?.avatar, let url = URL.init(string: avatar) {
            self.userImageView.af_setImage(withURL: url)
        }
    }
    
    // MARK: - Actions
    @objc private func logoutBtnTapped() {
        AppData.loginData = nil
        self.dismiss(animated: true)
    }
    
    // MARK: - Networking
    private func fetchProfileData(idUser: String) {
        viewModel.fetchProfileData(idUser: idUser)
        
        viewModel.updateLoading = {
            let _ = self.viewModel.isLoading ? self.loadingStart() : self.loadingStop()
        }
        
        viewModel.showAlert = {
            if let errorMsg = self.viewModel.errorMessage {
                self.view.makeToast(errorMsg, duration: 1.5, position: .bottom)
            }
        }
        
        viewModel.didFinishFetchProfile = {
            print("Success get profile user")
            self.updateDataView()
        }
    }

}
