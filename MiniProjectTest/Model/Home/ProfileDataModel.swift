//
//  ProfileDataModel.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation

public class ProfileDataModel {
    public var id : Int?
    public var email : String?
    public var first_name : String?
    public var last_name : String?
    public var avatar : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let profileDataModel_list = ProfileDataModel.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of ProfileDataModel Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ProfileDataModel]
    {
        var models:[ProfileDataModel] = []
        for item in array
        {
            models.append(ProfileDataModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let profileDataModel = ProfileDataModel(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: ProfileDataModel Instance.
*/
    required public init?(dictionary: NSDictionary) {

        id = dictionary["id"] as? Int
        email = dictionary["email"] as? String
        first_name = dictionary["first_name"] as? String
        last_name = dictionary["last_name"] as? String
        avatar = dictionary["avatar"] as? String
    }

        
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.email, forKey: "email")
        dictionary.setValue(self.first_name, forKey: "first_name")
        dictionary.setValue(self.last_name, forKey: "last_name")
        dictionary.setValue(self.avatar, forKey: "avatar")

        return dictionary
    }

}
