//
//  ProfileSupportModel.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation

public class ProfileSupportModel {
    public var url : String?
    public var text : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let profileSupportModel_list = ProfileSupportModel.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of ProfileSupportModel Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ProfileSupportModel]
    {
        var models:[ProfileSupportModel] = []
        for item in array
        {
            models.append(ProfileSupportModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let profileSupportModel = ProfileSupportModel(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: ProfileSupportModel Instance.
*/
    required public init?(dictionary: NSDictionary) {

        url = dictionary["url"] as? String
        text = dictionary["text"] as? String
    }

        
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.url, forKey: "url")
        dictionary.setValue(self.text, forKey: "text")

        return dictionary
    }

}
