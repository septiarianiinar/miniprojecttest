//
//  ProfileModel.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation

public class ProfileModel {
    
    public var data : ProfileDataModel?
    public var support : ProfileSupportModel?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let profileModel_list = ProfileModel.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of ProfileModel Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [ProfileModel]
    {
        var models:[ProfileModel] = []
        for item in array
        {
            models.append(ProfileModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let profileModel = ProfileModel(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: ProfileModel Instance.
*/
    required public init?(dictionary: NSDictionary) {

        if (dictionary["data"] != nil) { data = ProfileDataModel(dictionary: dictionary["data"] as! NSDictionary) }
        if (dictionary["support"] != nil) { support = ProfileSupportModel(dictionary: dictionary["support"] as! NSDictionary) }
    }

        
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.data?.dictionaryRepresentation(), forKey: "data")
        dictionary.setValue(self.support?.dictionaryRepresentation(), forKey: "support")

        return dictionary
    }

}
