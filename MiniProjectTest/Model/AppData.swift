//
//  AppData.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation

class AppData {
    
    class var loginData:LoginModel?{
        get {
            let identifier = "LoginModel"
            if let data = UserDefaults.standard.object(forKey: "\(Bundle.applicationName)-\(identifier)") as? Data, let value = NSKeyedUnarchiver.unarchiveObject(with: data) as? LoginModel{
                return value
            }
            return nil
        }
        set (newValue) {
            let identifier = "LoginModel"
            if let value = newValue {
                let loginData = NSKeyedArchiver.archivedData(withRootObject: value)
                UserDefaults.standard.setValue(loginData, forKey: "\(Bundle.applicationName)-\(identifier)")
            } else {
                UserDefaults.standard.removeObject(forKey: "\(Bundle.applicationName)-\(identifier)")
            }
            UserDefaults.standard.synchronize()
        }
    }
}
