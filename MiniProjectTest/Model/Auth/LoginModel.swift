//
//  LoginModel.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation

public class LoginModel: NSObject, NSCoding {
    
    public var id : Int?
    public var token : String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let loginModel_list = LoginModel.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of LoginModel Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [LoginModel]
    {
        var models:[LoginModel] = []
        for item in array
        {
            models.append(LoginModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let loginModel = LoginModel(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: LoginModel Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        id = dictionary["id"] as? Int
        token = dictionary["token"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.token, forKey: "token")
        
        return dictionary
    }
    
    @objc public required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        token = aDecoder.decodeObject(forKey: "token") as? String
    }
    
    public func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if token != nil{
            aCoder.encode(token, forKey: "token")
        }
    }
    
}
