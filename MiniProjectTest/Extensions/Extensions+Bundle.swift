//
//  Extensions+Bundle.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation

extension Bundle {
    
    class var applicationName: String {
        if let build = self.main.infoDictionary?["CFBundleName"] as? String {
            return build
        }
        return "Name Not Available"
    }
    
    class var applicationVersionNumber: String {
        if let version = self.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        return "Version Number Not Available"
    }
    
    class var applicationBuildNumber: String {
        if let build = self.main.infoDictionary?["CFBundleVersion"] as? String {
            return build
        }
        return "Build Number Not Available"
    }
    
    
}
