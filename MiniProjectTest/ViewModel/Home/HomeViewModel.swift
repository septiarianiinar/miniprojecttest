//
//  HomeViewModel.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation

class HomeViewModel {
    
    private var apiService: ApiService?
    var isLoading: Bool = false {
        didSet {
            self.updateLoading?()
        }
    }
    
    var errorMessage: String? {
        didSet {
            self.showAlert?()
        }
    }
    
    var profileData: ProfileModel? {
        didSet {
            guard let data = profileData else { return }
            self.didFinishFetchProfile?()
        }
    }
    
    // MARK: - Closured for callback
    var updateLoading: (() -> ())?
    var didFinishFetchProfile: (() -> ())?
    var showAlert: (() -> ())?
    
    // MARK: - Constructor
    init(apiService: ApiService) {
        self.apiService = apiService
    }
    
    // MARK: - Network Call
    func fetchProfileData(idUser: String) {
        self.isLoading = true
        self.apiService?.requestProfile(idUser: idUser, completionHandler: { (status, errorMsg, results) in
            switch status {
            case .success:
                
                if let result = results {
                    let resultData = ProfileModel(dictionary: result)
                    self.profileData = resultData
                    self.errorMessage = nil
                    self.isLoading = false
                    
                } else {
                    self.errorMessage = errorMsg
                    self.isLoading = false
                }
            case .failure:
                self.errorMessage = errorMsg
                self.isLoading = false
            }
        })
    }
    
}
