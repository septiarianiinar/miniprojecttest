//
//  LoginViewModel.swift
//  MiniProjectTest
//
//  Created by Rani Septiariani on 29/09/22.
//

import Foundation

class LoginViewModel {
    
    private var apiService: ApiService?
    var isLoading: Bool = false {
        didSet {
            self.updateLoading?()
        }
    }
    
    var errorMessage: String? {
        didSet {
            self.showAlert?()
        }
    }
    
    var loginData: LoginModel? {
        didSet {
            guard let data = loginData else { return }
            self.didFinishFetchLogin?()
        }
    }
    
    // MARK: - Closured for callback
    var updateLoading: (() -> ())?
    var didFinishFetchLogin: (() -> ())?
    var showAlert: (() -> ())?
    
    // MARK: - Constructor
    init(apiService: ApiService) {
        self.apiService = apiService
    }
    
    // MARK: - Network Call
    func fetchLogin(params: [String:Any]) {
        self.isLoading = true
        self.apiService?.requestLogin(params: params, completionHandler: { (status, errorMsg, results) in
            switch status {
            case .success:
                
                if let result = results {
                    let resultData = LoginModel(dictionary: result)
                    AppData.loginData = resultData
                    self.loginData = resultData
                    self.errorMessage = nil
                    self.isLoading = false
                    
                } else {
                    self.errorMessage = errorMsg
                    self.isLoading = false
                }
            case .failure:
                self.errorMessage = errorMsg
                self.isLoading = false
            }
        })
    }
    
}
